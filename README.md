**仍旧存在一些问题，但近期没有更新计划**

比如目前登录以后，后端没有处理登录信息

本项目用于2020年秋季数据库大作业



---

架子就这样吧, 该push就push.

前-后端接口在`Documents`里, 前端的实现写在`FrontEnd`文件夹里(顺便把那个`FrontEnd\labalaba.txt`给删了).

后端-数据库接口在`BackEnd\src\main\java\cn\edu\buaa\treehole\dao`里, 将接口的实现写在`BackEnd\src\main\java\cn\edu\buaa\treehole\dao\implement`中.

后端的接口定义还没写完.

前端往gitignore后面加点东西, 目前的gitignore仅针对java.

先checkout自己的分支, 然后写, 写完了push上来, 我来merge.

每个自己写的文件都要署名, 不要在自己的分支更改别人署名的文件.

