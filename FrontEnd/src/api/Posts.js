import request from '@/utils/request' //引入封装好的 axios 请求

export function searchData(username, password) { //登录接口
    return request({ //使用封装好的 axios 进行网络请求
        url: '/admin/login',
        method: 'post',
        data: { //提交的数据
            username,
            password
        }
    })
}

export function getHotData() { //登录接口
    return request({ //使用封装好的 axios 进行网络请求
        url: '/hot_list/get_list',
        method: 'post',
    })
}

export function getTimeData(time) { //登录接口
    return request({ //使用封装好的 axios 进行网络请求
        url: '/post_list/'+time,
        method: 'post',
    })
}

export function deleteData(pid, signature) { //登录接口
    return request({ //使用封装好的 axios 进行网络请求
        url: '/delete_post',
        method: 'post',
        data: { //提交的数据
            pid:pid,
            signature:signature
        }
    })
}

export function getsign(pid) { //登录接口
    return request({ //使用封装好的 axios 进行网络请求
        url: '/delete_post',
        method: 'post',
        data: { //提交的数据
            pid:pid,
            signature:signature
        }
    })
}

export function getDataDetails(pid) { //登录接口
    return request({ //使用封装好的 axios 进行网络请求
        url: '/post_info/'+pid,
        method: 'post',
    })
}

export function postNote(signature, title ,content, tags) { //登录接口
    //alert("last layer"+tags[0])
    return request({ //使用封装好的 axios 进行网络请求
        url: '/add_post',
        method: 'post',
        data: { //提交的数据
            signature:signature,
            title:title,
            content:content,
            tags: tags
        }
    })
}

export function postReply(pid, signature,content) { //登录接口
  //alert("postReply" + pid + signature + content)
  return request({ //使用封装好的 axios 进行网络请求
        url: '/add_reply',
        method: 'post',
        data: { //提交的数据
          pid: pid,
          signature:signature,
          content:content,
        }
    })
}

export function search(tag) { //登录接口
    //alert(tag)
    return request({ //使用封装好的 axios 进行网络请求
        url: '/search/str_tag_post?str='+tag,
        method: 'post',
        data: { //提交的数据
        }
    })
}

export function earlyReplyId(pid) {
  //alert("pid:"+pid)
  return request({ //使用封装好的 axios 进行网络请求
    url: '/reply_list/get_earliest?pid='+pid,
    method: 'post',
  })
}

export function getReplyList(pid, rid) {
  //alert('/reply_list/get_list?pid='+pid + '&pt=' + rid + '&num=100&front=true')
  return request({ //使用封装好的 axios 进行网络请求
    url: '/reply_list/get_list?pid='+pid + '&pt=' + rid + '&num=100&front=false',
    method: 'post',
  })
}

export function getPostList(pid, front) {
    //alert("1"+pid+front)
  return request({ //使用封装好的 axios 进行网络请求
    url: '/post_list/get_list?'+'&pt=' + pid + '&num=100&front='+front,
    method: 'post',
  })
}

export function getReply(pid, rid) {
    //alert(pid+rid)
  return request({ //使用封装好的 axios 进行网络请求
    url: '/reply_info/' + pid + '/'+rid,
    method: 'post',
  })
}

export function userregist(email, password, default_signature) { //登录接口
    //alert(email+password+default_signature)
    return request({ //使用封装好的 axios 进行网络请求
        url: '/user/sign_up',
        method: 'post',
        data: { //提交的数据
            email,
            password,
            default_signature
        }
    })
}
