import request from '@/utils/request' //引入封装好的 axios 请求

export function login(email, password) { //登录接口
    //alert(username+password)
    return request({ //使用封装好的 axios 进行网络请求
        url: '/user/login',
        method: 'post',
        data: { //提交的数据
            email,
            password
        }
    })
}

export function userregist(username, password, default_signature) { //登录接口
    alert(username, password, default_signature)
    return request({ //使用封装好的 axios 进行网络请求
    url: '/user/sign_up',
    method: 'post',
    data: { //提交的数据
      username,
      password,
        default_signature
    }
  })
}

export function getData(pid) { //登录接口
    return request({ //使用封装好的 axios 进行网络请求
        url: '/post_info/'+pid,
        method: 'get',
        data: { //提交的数据
            pid:pid,
        }
    })
}

