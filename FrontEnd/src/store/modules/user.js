import { login} from '@/api/login'//引入登录 api 接口
import { getData } from '@/api/login'//引入登录 api 接口
import {
    getHotData,
    deleteData,
    getTimeData,
    postNote,
    postReply,
    earlyReplyId,
    getPostList,
    getReplyList,
    getReply,
    userregist,
    search, getsign,
} from '@/api/Posts'

const user = {
    actions: {
        // 登录
        Login({ commit }, userInfo) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
            //alert(userInfo.username)
            const username = userInfo.username.trim()
            return new Promise((resolve, reject) => { //封装一个 Promise
                login(username, userInfo.password).then(response => { //使用 login 接口进行网络请求
                    commit('') //提交一个 mutation，通知状态改变
                    resolve(response) //将结果封装进 Promise
                }).catch(error => {
                    reject(error)
                })
            })
        },
      Regist({ commit }, userInfo) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
        const username = userInfo.username.trim()
          const password = userInfo.password.trim()
          const signature = userInfo.signature.trim()
          //alert(username+password+signature)
        return new Promise((resolve, reject) => { //封装一个 Promise
          userregist(username, password, signature).then(response => { //使用 login 接口进行网络请求
            commit('') //提交一个 mutation，通知状态改变
            resolve(response) //将结果封装进 Promise
          }).catch(error => {
            reject(error)
          })
        })
      },
      GetEarlyReplyId({ commit }, pid) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
          //alert("GetEarPid")
          return new Promise((resolve, reject) => { //封装一个 Promise
          earlyReplyId(pid).then(response => { //使用 login 接口进行网络请求
            commit('') //提交一个 mutation，通知状态改变
            resolve(response) //将结果封装进 Promise
          }).catch(error => {
            reject(error)
          })
        })
      },
      GetReply({ commit }, info) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
        return new Promise((resolve, reject) => { //封装一个 Promise
          getReply(info[0], info[1]).then(response => { //使用 login 接口进行网络请求
            commit('') //提交一个 mutation，通知状态改变
            resolve(response) //将结果封装进 Promise
          }).catch(error => {
            reject(error)
          })
        })
      },
      GetReplyList({ commit }, pac) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
          //alert("get reply list")
          return new Promise((resolve, reject) => { //封装一个 Promise
          getReplyList(pac.pid, pac.rid).then(response => { //使用 login 接口进行网络请求
            commit('') //提交一个 mutation，通知状态改变
            resolve(response) //将结果封装进 Promise
          }).catch(error => {
            reject(error)
          })
        })
      },
      GetPostList({ commit }, info) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
            //alert(""+info.pid+info.front)
            return new Promise((resolve, reject) => { //封装一个 Promise
          getPostList(info.pid, info.front).then(response => { //使用 login 接口进行网络请求
            commit('') //提交一个 mutation，通知状态改变
            resolve(response) //将结果封装进 Promise
          }).catch(error => {
            reject(error)
          })
        })
      },
      GetData({ commit }, pid) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
          //const username = userInfo.username.trim()
          return new Promise((resolve, reject) => { //封装一个 Promise
              getData(pid).then(response => { //使用 login 接口进行网络请求
                  commit('u') //提交一个 mutation，通知状态改变
                  resolve(response) //将结果封装进 Promise
              }).catch(error => {
                  reject(error)
              })
          })
      },
      GetHotData({ commit },) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
          return new Promise((resolve, reject) => { //封装一个 Promise
              getHotData().then(response => { //使用 login 接口进行网络请求
                  commit('u') //提交一个 mutation，通知状态改变
                  resolve(response) //将结果封装进 Promise
              }).catch(error => {
                  reject(error)
              })
          })
      },
      GetTimeData({ commit }, type) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
          return new Promise((resolve, reject) => { //封装一个 Promise
              getTimeData(type).then(response => { //使用 login 接口进行网络请求
                  commit('') //提交一个 mutation，通知状态改变
                  resolve(response) //将结果封装进 Promise
              }).catch(error => {
                  reject(error)
              })
          })
      },
      DeleteData({ commit }, userInfo) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
          return new Promise((resolve, reject) => { //封装一个 Promise
              deleteData(userInfo.deletepid, userInfo.signature).then(response => { //使用 login 接口进行网络请求
                  commit('') //提交一个 mutation，通知状态改变
                  resolve(response) //将结果封装进 Promise
              }).catch(error => {
                  reject(error)
              })
          })
      },
        GetSign({ commit }, userInfo) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
            new Promise((resolve, reject) => { //封装一个 Promise
                getsign(userInfo.deletepid).then(response => { //使用 login 接口进行网络请求
                    commit('') //提交一个 mutation，通知状态改变
                    resolve(response) //将结果封装进 Promise
                }).catch(error => {
                    reject(error)
                })
            })
        },
      GetDataDetails({ commit }, userInfo) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
          const username = userInfo.username.trim()
          return new Promise((resolve, reject) => { //封装一个 Promise
              getData(username, userInfo.password).then(response => { //使用 login 接口进行网络请求
                  commit('') //提交一个 mutation，通知状态改变
                  resolve(response) //将结果封装进 Promise
              }).catch(error => {
                  reject(error)
              })
          })
      },
      PostNote({ commit }, userInfo) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
            const username = userInfo.signature.trim()
          //alert(username)
          const title = userInfo.title.trim()
          const content = userInfo.content.trim()
          const tags = userInfo.tags
          //alert(""+tags[0])
          return new Promise((resolve, reject) => { //封装一个 Promise
              postNote(username, title, content, tags).then(response => { //使用 login 接口进行网络请求
                  commit('') //提交一个 mutation，通知状态改变
                  resolve(response) //将结果封装进 Promise
              }).catch(error => {
                  reject(error)
              })
          })
      },
      PostReply({ commit }, info) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
          //alert("PostReply" + info[0]+ info[1] + info[2])
          return new Promise((resolve, reject) => { //封装一个 Promise
              postReply(info[0], info[1], info[2]).then(response => { //使用 login 接口进行网络请求
                  commit('') //提交一个 mutation，通知状态改变
                  resolve(response) //将结果封装进 Promise
              }).catch(error => {
                  reject(error)
              })
          })
      },
    Search({ commit }, info) { //定义 Login 方法，在组件中使用 this.$store.dispatch("Login") 调用
        //alert("PostReply" + info[0]+ info[1] + info[2])
        //alert(info)
        return new Promise((resolve, reject) => { //封装一个 Promise
            search(info).then(response => { //使用 login 接口进行网络请求
                commit('') //提交一个 mutation，通知状态改变
                resolve(response) //将结果封装进 Promise
            }).catch(error => {
                reject(error)
            })
        })
    },
    }
}
export default user

