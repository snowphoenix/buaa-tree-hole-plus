import Vue from 'vue' //引入 Vue
import Vuex from 'vuex' //引入 Vuex
import user from './modules/user' //引入 user module

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        user //使用 user.js 中的 action
    },
    state: {
      count: 1,
      dialogVisible: false,
      login: true,
      postDataType: "hot",
      username: "123",
      signature: "123",
        footerStyle:"",
    },
    mutations: {
      // 事件类型 type 为 increment
      u (state) {
        // 变更状态
        state.count++
      },
      visible(state) {
        state.dialogVisible = true;
      }
    }
})

export default store

