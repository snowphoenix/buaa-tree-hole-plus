package cn.edu.buaa.treehole.pojo.dao;

import lombok.Data;

@Data
public class UserDaoInfo {
    private long uid;
    private String email;
    private String password;
    private String auto;
}
