package cn.edu.buaa.treehole.dao;

import cn.edu.buaa.treehole.dao.exception.DaoException;
import cn.edu.buaa.treehole.dao.exception.PidNotExistException;
import cn.edu.buaa.treehole.dao.exception.RidHasExistException;
import cn.edu.buaa.treehole.dao.exception.RidNotExistException;
import cn.edu.buaa.treehole.pojo.ReplyInfo;
import cn.edu.buaa.treehole.pojo.dao.ReplyDaoInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
 * @author Deng XinYu
 * */
@Repository
public interface ReplyDao {


    //@Select("select * from reply where pid=#{pid} and rid=#{rid}")
    @Select("select * from reply where pid=#{pid} and rid=#{rid}")
    ReplyDaoInfo selectReplyById(@Param("pid") long pid, @Param("rid") long rid);


    @Select("select * from reply where pid=#{pid}")
    List<ReplyDaoInfo> selectAllReplyByPid(@Param("pid") long pid);


    @Select("select max(rid) from reply where pid=#{pid}")
    long selectMaxRidByPid(@Param("pid") long pid);


    @Select("select min(rid) from reply where pid=#{pid}")
    long selectMinRidByPid(@Param("pid") long pid);


    //@Insert("insert into reply (pid, rid, time, signature) values " +
    //        "(#{info.pid}, #{info.rid}, #{info.time}, #{info.signature})")
    //void insertReply(@Param("info") ReplyDaoInfo replyDaoInfo);
    @Insert("insert into reply (pid, rid, time, signature) values" + 
             "(#{pid}, #{rid}, #{time}, #{signature})")
    void insertReply(ReplyDaoInfo replyDaoInfo);

    @Delete("delete from reply where pid=#{pid} and rid=#{rid}")
    void deleteReplyById(@Param("pid") long pid, @Param("rid") long rid);

    @Delete("delete from reply where pid=#{pid}")
    void deleteAllReplyByPid(@Param("pid") long pid);
}