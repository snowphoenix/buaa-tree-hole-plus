package cn.edu.buaa.treehole.service.exception;

public class PasswordWrongException extends Exception {
    public PasswordWrongException() {
        super();
    }

    public PasswordWrongException(String msg) {
        super(msg);
    }

    public PasswordWrongException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public PasswordWrongException(Throwable cause) {
        super(cause);
    }
}
