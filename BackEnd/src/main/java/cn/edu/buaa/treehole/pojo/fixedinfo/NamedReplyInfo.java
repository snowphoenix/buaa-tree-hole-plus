package cn.edu.buaa.treehole.pojo.fixedinfo;

import java.util.Date;

public class NamedReplyInfo {
    private long pid;
    private long rid;
    private String content;
    private String name;
    private Date date;

    public NamedReplyInfo() { }

    public NamedReplyInfo(long pid, long rid, String content, String name, Date date) {
        this.pid = pid;
        this.rid = rid;
        this.content = content;
        this.name = name;
        this.date = date;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public long getRid() {
        return rid;
    }

    public void setRid(long rid) {
        this.rid = rid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "NamedReplyInfo{" +
                "pid=" + pid +
                ", rid=" + rid +
                ", content='" + content + '\'' +
                ", name='" + name + '\'' +
                ", date=" + date +
                '}';
    }
}
