package cn.edu.buaa.treehole.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
public class DefaultSetting implements Settings {
    @Override
    public String getAdminSignature() {
        return "admin";
    }
}
