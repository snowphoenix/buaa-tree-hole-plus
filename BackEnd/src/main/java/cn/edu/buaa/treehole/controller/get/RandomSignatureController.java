package cn.edu.buaa.treehole.controller.get;

import cn.edu.buaa.treehole.service.TimeService;
import cn.edu.buaa.treehole.dao.exception.DaoException;
import cn.edu.buaa.treehole.pojo.network.get.RandomSignatureNetworkInfo;
import cn.edu.buaa.treehole.service.SignatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 * @author Deng XinYu
 * */
@Controller
@RequestMapping("/random_signature")
public class RandomSignatureController {
    private final SignatureService signatureService;
    private final TimeService timeService;

    @Autowired
    public RandomSignatureController(SignatureService signatureService, TimeService timeService) {
        this.signatureService = signatureService;
        this.timeService = timeService;
    }


    @RequestMapping("/{pid}")
    @ResponseBody
    public RandomSignatureNetworkInfo getRandomSignature(@PathVariable long pid) {
        RandomSignatureNetworkInfo out = new RandomSignatureNetworkInfo();

        try {
            String signature = signatureService.nextSignature(pid);
            out.setSignature(signature);
            out.setStatus("success");
        } catch (Exception e) {
            out.setStatus("fail");
        }
        out.setTime(timeService.nowTime());

        return out;
    }
}
