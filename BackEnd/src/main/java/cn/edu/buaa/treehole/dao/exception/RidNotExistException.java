package cn.edu.buaa.treehole.dao.exception;

/*
 * @author Deng XinYu
 * */
public class RidNotExistException extends DaoException {
    public RidNotExistException() {
    }

    public RidNotExistException(String msg) {
        super(msg);
    }

    public RidNotExistException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public RidNotExistException(Throwable cause) {
        super(cause);
    }

    protected RidNotExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
