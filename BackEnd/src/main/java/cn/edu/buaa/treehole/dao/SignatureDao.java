package cn.edu.buaa.treehole.dao;

import cn.edu.buaa.treehole.dao.exception.*;
import cn.edu.buaa.treehole.pojo.dao.SignatureDaoInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/*
* @author Deng XinYu
* */
@Repository
public interface SignatureDao {


    @Select("select nid from signature where PID=#{pid} and SIGNATURE=#{signature}")
    int selectNidByIdAndSignature(@Param("pid") long pid, @Param("signature") String signature);


    @Select("select nid from signature where PID=#{pid} and SIGNATURE=#{signature}")
    List<Integer> selectAllNidByIdAndSignature(@Param("pid") long pid, @Param("signature") String signature);

    @Select("select signature from signature where PID=#{pid} and NID=#{nid}")
    String selectSignatureById(@Param("pid") long pid, @Param("nid") long nid);


    @Insert("insert into signature (pid, signature, nid) values (#{pid}, #{signature}, #{nid})")
    void insertSignature(SignatureDaoInfo info);


    @Select("select max(nid) from signature where pid=#{pid}")
    int selectMaxNidByPid(@Param("pid") long pid);
}