package cn.edu.buaa.treehole.pojo.dao;

import lombok.Data;

@Data
public class PostDaoInfo {
    private long pid;
    private String title;

    public PostDaoInfo() {}

    public void setPid(long pid) {
        this.pid = pid;
    }

    public long getPid() {
        return pid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
