package cn.edu.buaa.treehole.pojo.network.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UserSignUpRequestInfo {
    String email;
    String password;

    @JsonProperty("default_signature")
    String defaultSignature;

    public UserSignUpRequestInfo() {}

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public void setDefaultSignature(String defaultSignature) {
        this.defaultSignature = defaultSignature;
    }

    public String getDefaultSignature() {
        return defaultSignature;
    }
}
