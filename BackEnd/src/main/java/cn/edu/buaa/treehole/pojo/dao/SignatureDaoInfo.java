package cn.edu.buaa.treehole.pojo.dao;

import lombok.Data;

@Data
public class SignatureDaoInfo {
    private long pid;
    private String signature;
    private long nid;

    public SignatureDaoInfo() {}

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSignature() {
        return signature;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public long getPid() {
        return pid;
    }

    public void setNid(long nid) {
        this.nid = nid;
    }

    public long getNid() {
        return nid;
    }
}
