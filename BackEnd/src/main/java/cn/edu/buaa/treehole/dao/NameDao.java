package cn.edu.buaa.treehole.dao;

import cn.edu.buaa.treehole.pojo.dao.NameDaoInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NameDao {
    @Select("select * from name")
    List<NameDaoInfo> selectAllName();

    @Insert("insert into name (nid, name) values (#{nid}, #{name})")
    void insertName(NameDaoInfo nameDaoInfo);
}
