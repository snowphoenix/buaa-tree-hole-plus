package cn.edu.buaa.treehole.dao.exception;

public class SignatureHasExistException extends DaoException {
    public SignatureHasExistException() {
    }

    public SignatureHasExistException(String msg) {
        super(msg);
    }

    public SignatureHasExistException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public SignatureHasExistException(Throwable cause) {
        super(cause);
    }

    protected SignatureHasExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
