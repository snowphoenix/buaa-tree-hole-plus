package cn.edu.buaa.treehole.dao;

import cn.edu.buaa.treehole.pojo.dao.ContentDaoInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentDao {
    @Select("select * from content where pid=#{pid} and rid=#{rid} order by ord")
    List<ContentDaoInfo> selectAllAscendingContentById(@Param("pid") long pid, @Param("rid") long rid);

    @Select("select * from content where pid=#{pid} and rid=0")
    ContentDaoInfo selectFirstContentById(@Param("pid") long pid);

    @Insert("insert into content (pid, rid, ord, contents) values (#{pid}, #{rid}, #{ord}, #{contents})")
    void insertContent(ContentDaoInfo contentDaoInfo);

    @Delete("delete from content where pid=#{pid}")
    void deleteAllPostContentByPid(@Param("pid") long pid);

    @Delete("delete from content where pid=#{pid} and rid=#{rid}")
    void deleteAllReplyContentById(@Param("pid") long pid, @Param("rid") long rid);
}
