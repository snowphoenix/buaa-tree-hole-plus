package cn.edu.buaa.treehole.service.exception;

public class EmailTooLongException extends Exception {
    public EmailTooLongException() {
        super();
    }

    public EmailTooLongException(String msg) {
        super(msg);
    }

    public EmailTooLongException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public EmailTooLongException(Throwable cause) {
        super(cause);
    }
}