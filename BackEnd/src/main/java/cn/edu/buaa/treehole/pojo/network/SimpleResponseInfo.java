package cn.edu.buaa.treehole.pojo.network;

import lombok.Data;

@Data
public class SimpleResponseInfo {
    String status;
    String time;

    public SimpleResponseInfo() {}

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public String getTime() {
        return time;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
