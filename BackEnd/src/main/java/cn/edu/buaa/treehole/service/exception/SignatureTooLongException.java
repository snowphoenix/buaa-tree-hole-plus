package cn.edu.buaa.treehole.service.exception;

public class SignatureTooLongException extends Exception {
    public SignatureTooLongException() {
        super();
    }

    public SignatureTooLongException(String msg) {
        super(msg);
    }

    public SignatureTooLongException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public SignatureTooLongException(Throwable cause) {
        super(cause);
    }
}
