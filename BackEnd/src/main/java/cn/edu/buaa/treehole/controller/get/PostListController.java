package cn.edu.buaa.treehole.controller.get;

import cn.edu.buaa.treehole.service.TimeService;
import cn.edu.buaa.treehole.dao.exception.DaoException;
import cn.edu.buaa.treehole.dao.exception.PidNotExistException;
import cn.edu.buaa.treehole.pojo.network.get.LatestOrEarliestPostNetworkInfo;
import cn.edu.buaa.treehole.service.PostListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/*
 * @author Deng XinYu
 * */
@Controller
@RequestMapping("/post_list")
public class PostListController {
    private final PostListService postListService;
    private final TimeService timeService;
    private final Logger logger = LoggerFactory.getLogger(PostListController.class);

    @Autowired
    public PostListController(PostListService postListService, TimeService timeService) {
        this.postListService = postListService;
        this.timeService = timeService;
    }

    @RequestMapping("/get_latest")
    @ResponseBody
    public LatestOrEarliestPostNetworkInfo getLatest()
    {
        LatestOrEarliestPostNetworkInfo out = new LatestOrEarliestPostNetworkInfo();

        try {
            long pid = postListService.getLatest();
            out.setPid(pid);
            out.setStatus("success");
        } catch (PidNotExistException e) {
            out.setStatus("null");
        } catch (Exception e) {
            out.setStatus("exception");
        }
        out.setFreshTime(timeService.nowTime());

        return out;
    }

    @RequestMapping("/get_earliest")
    @ResponseBody
    public LatestOrEarliestPostNetworkInfo getEarliest()
    {
        LatestOrEarliestPostNetworkInfo out = new LatestOrEarliestPostNetworkInfo();

        try {
            long pid = postListService.getEarliest();
            out.setPid(pid);
            out.setStatus("success");
        } catch (PidNotExistException e) {
            out.setStatus("null");
        } catch (DaoException e) {
            out.setStatus("exception");
        }
        out.setFreshTime(timeService.nowTime());

        return out;
    }

    @RequestMapping("/get_list")
    @ResponseBody
    public List<Long> getList(
            @RequestParam(value = "pt") long pt,
            @RequestParam(value = "num") int num,
            @RequestParam(value = "front") boolean front
    ) {
        List<Long> out;

        try {
            if (front) {
                out = postListService.backwardList(pt, num);
            }
            else {
                out = postListService.forwardList(pt, num);
            }
            return out;
        } catch (PidNotExistException ignored) {

        } catch (DaoException e) {
            logger.error(toString() +
                    ".getList(" + pt + ", " + num + ", " + front + ")");
        }
        return new ArrayList<>();
    }
}
