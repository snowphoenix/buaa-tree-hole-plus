package cn.edu.buaa.treehole.pojo.dao;

import lombok.Data;

import java.util.Date;

@Data
public class ReplyDaoInfo {
    private long pid;
    private long rid;
    private Date time;
    private String signature;
}
