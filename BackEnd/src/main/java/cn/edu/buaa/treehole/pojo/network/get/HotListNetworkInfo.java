package cn.edu.buaa.treehole.pojo.network.get;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/*
 * @author Deng XinYu
 * */
public class HotListNetworkInfo {
    private String status;

    @JsonProperty("fresh_time")
    private String freshTime;

    @JsonProperty("pid_list")
    private List<Long> pidList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Long> getPidList() {
        return pidList;
    }

    public void setPidList(List<Long> pidList) {
        this.pidList = pidList;
    }

    public String getFreshTime() {
        return freshTime;
    }

    public void setFreshTime(String freshTime) {
        this.freshTime = freshTime;
    }
}
