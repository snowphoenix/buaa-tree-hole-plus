package cn.edu.buaa.treehole.pojo.fixedinfo;

import java.util.Date;
import java.util.Objects;

public class ComparablePostInfo implements Comparable<ComparablePostInfo> {
    private long pid;
    private Date date;
    private int replyNum;

    public ComparablePostInfo() { }

    public ComparablePostInfo(long pid, Date date, int replyNum) {
        this.pid = pid;
        this.date = date;
        this.replyNum = replyNum;
    }

    public ComparablePostInfo(Date date, int replyNum) {
        this.date = date;
        this.replyNum = replyNum;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getReplyNum() {
        return replyNum;
    }

    public void setReplyNum(int replyNum) {
        this.replyNum = replyNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComparablePostInfo that = (ComparablePostInfo) o;
        return pid == that.pid &&
                replyNum == that.replyNum &&
                Objects.equals(date, that.date);
    }

    @Override
    public String toString() {
        return "ComparablePostInfo{" +
                "pid=" + pid +
                ", date=" + date +
                ", replyNum=" + replyNum +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(pid, date, replyNum);
    }

    /*
    * 回复数量更少、更新时间更早的Info，排序靠后
    * */
    @Override
    public int compareTo(ComparablePostInfo o) {
        if (replyNum > o.replyNum) {
            return -1;
        }
        if (replyNum == o.replyNum) {
            return -date.compareTo(o.date);
        }
        return 1;
    }
}
