package cn.edu.buaa.treehole.dao;

import cn.edu.buaa.treehole.pojo.dao.UserSignatureDaoInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PatchMapping;

import java.util.List;

@Repository
public interface UserSignatureDao {
    @Insert("insert into usig (pid, signature, uid, time) values(#{pid}, " +
            "#{signature}, #{uid}, #{time})")
    void insertUserSignature(UserSignatureDaoInfo userSignatureDaoInfo);

    @Delete("delete from usig where uid=#{uid} and pid=#{pid} and signature=#{signature}")
    void deleteUserSignature(@Param("uid") long uid, @Param("pid") long pid, @Param("signature") String signature);

    @Select("select * from usig where uid=#{uid}")
    List<UserSignatureDaoInfo> selectAllSignatureByUid(@Param("uid") long uid);

    @Select("select * from usig where uid=#{uid} and pid=#{pid}")
    List<UserSignatureDaoInfo> selectAllSignatureById(@Param("uid") long uid, @Param("pid") long pid);
}
