package cn.edu.buaa.treehole.dao.dbutil;

public class DbAccess {

    /*
     * The url of database.
     */
    public static final String url = "jdbc:mysql://localhost:3306/treehole?serverTimezone=UTC";

    /*
     * The default username for accessing the database.
     */
    public static final String usr = "buaa";

    /*
     * The corresponding password.
     */
    public static final String pwd = "buaadb";
}
