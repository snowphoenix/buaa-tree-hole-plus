package cn.edu.buaa.treehole.controller.get;

import cn.edu.buaa.treehole.service.TimeService;
import cn.edu.buaa.treehole.dao.exception.DaoException;
import cn.edu.buaa.treehole.dao.exception.PidNotExistException;
import cn.edu.buaa.treehole.pojo.network.get.HotListNetworkInfo;
import cn.edu.buaa.treehole.service.HotListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/*
 * @author Deng XinYu
 * */
@Controller
@RequestMapping("/hot_list")
public class HotListController {
    private final HotListService hotListService;
    private final TimeService timeService;

    @Autowired
    public HotListController(HotListService hotListService, TimeService timeService) {
        this.hotListService = hotListService;
        this.timeService = timeService;
    }

    @RequestMapping("/get_list")
    @ResponseBody
    public HotListNetworkInfo getList()
    {
        HotListNetworkInfo out = new HotListNetworkInfo();

        try {
            List<Long> hotList = hotListService.getList();
            out.setFreshTime(timeService.format(hotListService.getFreshTime()));
            out.setPidList(hotList);
            out.setStatus("success");
        } catch (PidNotExistException e) {
            out.setStatus("null");
        } catch (Exception e) {
            out.setStatus("exception");
        }

        return out;
    }
}
