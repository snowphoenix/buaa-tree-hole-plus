package cn.edu.buaa.treehole.pojo.dao;

import lombok.Data;

@Data
public class NameDaoInfo {
    private int nid;
    private String name;

    public NameDaoInfo() {};

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNid(int nid) {
        this.nid = nid;
    }

    public int getNid() {
        return nid;
    }
}
