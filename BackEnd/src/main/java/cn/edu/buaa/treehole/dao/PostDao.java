package cn.edu.buaa.treehole.dao;

import cn.edu.buaa.treehole.dao.exception.DaoException;
import cn.edu.buaa.treehole.dao.exception.PidHasExistException;
import cn.edu.buaa.treehole.dao.exception.PidNotExistException;
import cn.edu.buaa.treehole.pojo.PostInfo;
import cn.edu.buaa.treehole.pojo.dao.PostDaoInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/*
 * @author Deng XinYu
 * */
@Repository
public interface PostDao {
    @Insert("insert into post (pid, title) values(#{pid}, #{title})")
    void insertPost(PostDaoInfo postDaoInfo);

    @Select("select * from post where pid=#{pid}")
    PostDaoInfo selectPostByPid(@Param("pid") long pid) throws PidNotExistException, DaoException;

    @Select("select * from post where pid=#{pid}")
    List<PostDaoInfo> selectAllPostByPid(@Param("pid") long pid) throws PidNotExistException, DaoException;

    @Delete("delete from post where pid=#{pid}")
    void deletePostByPid(@Param("pid") long pid) throws PidNotExistException, DaoException;

    @Select("select max(pid) from post")
    long selectMaxPid();
}