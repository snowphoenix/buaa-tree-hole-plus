package cn.edu.buaa.treehole.pojo.network.post;

/*
 * @author Deng XinYu
 * */
public class AddReplyRequestNetworkInfo {
    private long pid;
    private String signature;
    private String content;

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
