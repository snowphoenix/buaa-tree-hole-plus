package cn.edu.buaa.treehole.pojo.network.post;

import lombok.Data;

import java.util.List;

/*
 * @author Deng XinYu
 * */
@Data
public class AddPostRequestNetworkInfo {
    private String signature;
    private String title;
    private String content;
    private List<String> tags;
}
