package cn.edu.buaa.treehole.controller.post;

import cn.edu.buaa.treehole.service.TimeService;
import cn.edu.buaa.treehole.common.Pair;
import cn.edu.buaa.treehole.dao.exception.DaoException;
import cn.edu.buaa.treehole.dao.exception.SignatureNotExistException;
import cn.edu.buaa.treehole.pojo.ReplyInfo;
import cn.edu.buaa.treehole.pojo.network.post.AddReplyRequestNetworkInfo;
import cn.edu.buaa.treehole.pojo.network.post.AddReplyResponseNetworkInfo;
import cn.edu.buaa.treehole.pojo.network.post.DeleteReplyRequestNetworkInfo;
import cn.edu.buaa.treehole.pojo.network.post.DeleteReplyResponseNetworkInfo;
import cn.edu.buaa.treehole.service.ReplyInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/*
 * @author Deng XinYu
 * */
@Controller
public class AddDeleteReplyController {
    private final ReplyInfoService replyInfoService;
    private final TimeService timeService;
    private final Logger logger = LoggerFactory.getLogger(AddDeleteReplyController.class);

    @Autowired
    public AddDeleteReplyController(ReplyInfoService replyInfoService, TimeService timeService) {
        this.replyInfoService = replyInfoService;
        this.timeService = timeService;
    }

    @RequestMapping(value = "/add_reply", method = RequestMethod.POST)
    @ResponseBody
    public AddReplyResponseNetworkInfo addReply(@RequestBody AddReplyRequestNetworkInfo in, HttpServletRequest request) {
        AddReplyResponseNetworkInfo out = new AddReplyResponseNetworkInfo();
        System.out.println(in.getPid()+in.getContent()+in.getSignature());
        ReplyInfo info = new ReplyInfo();
        info.setPid(in.getPid());
        info.setContent(in.getContent());
        info.setSignature(in.getSignature());
        Date time = new Date();
        out.setTime(timeService.format(time));
        info.setDate(time);
        try {
            Pair<String, Long> result = replyInfoService.addReply(info);
            out.setName(result.getV1());
            out.setRid(result.getV2());
            out.setStatus("success");
        } catch (Exception e) {
            out.setStatus("fail");
            logger.error("addReply(" + in + ")", e);
        }

        return out;
    }

    @RequestMapping(value = "/delete_reply", method = RequestMethod.POST)
    @ResponseBody
    public DeleteReplyResponseNetworkInfo deleteReply(@RequestBody DeleteReplyRequestNetworkInfo in) {
        DeleteReplyResponseNetworkInfo out = new DeleteReplyResponseNetworkInfo();

        out.setTime(timeService.nowTime());
        try {
            replyInfoService.deleteReply(in.getPid(), in.getRid(), in.getSignature());
            out.setStatus("success");
        } catch (SignatureNotExistException e) {
            out.setStatus("wrong");
        } catch (Exception e) {
            out.setStatus("exception");
        }

        return out;
    }
}
