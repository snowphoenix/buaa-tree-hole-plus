package cn.edu.buaa.treehole.dao.exception;

public class ConnectionNotExistException extends Exception {

    public ConnectionNotExistException() {
        super();
    }

    public ConnectionNotExistException(String msg) {
        super(msg);
    }

    public ConnectionNotExistException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public ConnectionNotExistException(Throwable cause) {
        super(cause);
    }

    protected ConnectionNotExistException(String msg, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(msg, cause, enableSuppression, writableStackTrace);
    }
}
