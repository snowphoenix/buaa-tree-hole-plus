package cn.edu.buaa.treehole.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/*
 * @author Deng XinYu
 * */
@Data
public class PostInfo {
    private long pid;
    private List<Long> tidList;
    private String title;
    private String content;
    private String signature;
    private Date date;

    public PostInfo() {}

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSignature() {
        return signature;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public long getPid() {
        return pid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public List<Long> getTidList() {
        return tidList;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTidList(List<Long> tidList) {
        this.tidList = tidList;
    }
}
