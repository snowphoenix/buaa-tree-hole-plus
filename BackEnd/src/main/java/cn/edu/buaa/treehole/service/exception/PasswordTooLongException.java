package cn.edu.buaa.treehole.service.exception;

public class PasswordTooLongException extends Exception {
    public PasswordTooLongException() {
        super();
    }

    public PasswordTooLongException(String msg) {
        super(msg);
    }

    public PasswordTooLongException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public PasswordTooLongException(Throwable cause) {
        super(cause);
    }
}
