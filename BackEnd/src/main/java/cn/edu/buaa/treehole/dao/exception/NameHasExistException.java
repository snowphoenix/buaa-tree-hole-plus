package cn.edu.buaa.treehole.dao.exception;

public class NameHasExistException extends DaoException {
    public NameHasExistException() {
    }

    public NameHasExistException(String msg) {
        super(msg);
    }

    public NameHasExistException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public NameHasExistException(Throwable cause) {
        super(cause);
    }

    protected NameHasExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
