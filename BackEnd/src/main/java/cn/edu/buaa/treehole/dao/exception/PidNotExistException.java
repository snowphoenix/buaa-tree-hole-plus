package cn.edu.buaa.treehole.dao.exception;

/*
 * @author Deng XinYu
 * */
public class PidNotExistException extends DaoException {
    public PidNotExistException() {
    }

    public PidNotExistException(String msg) {
        super(msg);
    }

    public PidNotExistException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public PidNotExistException(Throwable cause) {
        super(cause);
    }

    protected PidNotExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
