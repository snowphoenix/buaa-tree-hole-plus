package cn.edu.buaa.treehole.controller.get;

import cn.edu.buaa.treehole.service.TimeService;
import cn.edu.buaa.treehole.dao.exception.DaoException;
import cn.edu.buaa.treehole.dao.exception.PidNotExistException;
import cn.edu.buaa.treehole.dao.exception.RidNotExistException;
import cn.edu.buaa.treehole.pojo.network.get.LatestOrEarliestReplyNetworkInfo;
import cn.edu.buaa.treehole.service.ReplyListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedList;
import java.util.List;

/*
 * @author Deng XinYu
 * */
@Controller
@RequestMapping("/reply_list")
public class ReplyListController {
    private final ReplyListService replyListService;
    private final TimeService timeService;
    private final Logger logger = LoggerFactory.getLogger(ReplyInfoController.class);

    @Autowired
    public ReplyListController(ReplyListService replyListService, TimeService timeService) {
        this.replyListService = replyListService;
        this.timeService = timeService;
    }

    @Override
    public String toString() {
        return "ReplyListController{" +
                "replyListService=" + replyListService +
                '}';
    }

    @RequestMapping("/get_latest")
    @ResponseBody
    public LatestOrEarliestReplyNetworkInfo getLatest(@RequestParam(value = "pid") long pid)
    {
        LatestOrEarliestReplyNetworkInfo out = new LatestOrEarliestReplyNetworkInfo();

        try {
            long rid = replyListService.getLatestReply(pid);
            out.setRid(rid);
            out.setStatus("success");
        } catch (RidNotExistException e) {
            out.setStatus("null");
        } catch (Exception e) {
            out.setStatus("exception");
        }

        out.setFreshTime(timeService.nowTime());

        return out;
    }

    @RequestMapping("/get_earliest")
    @ResponseBody
    public LatestOrEarliestReplyNetworkInfo getEarliest(@RequestParam(value = "pid") long pid)
    {
        LatestOrEarliestReplyNetworkInfo out = new LatestOrEarliestReplyNetworkInfo();

        try {
            long rid = replyListService.getEarliestReply(pid);
            out.setRid(rid);
            out.setStatus("success");
        } catch (RidNotExistException e) {
            out.setStatus("null");
        } catch (Exception e) {
            out.setStatus("exception");
        }

        out.setFreshTime(timeService.nowTime());

        return out;
    }

    @RequestMapping("get_list")
    @ResponseBody
    public List<Long> getList(
        @RequestParam(value = "pid") long pid,
        @RequestParam(value = "pt") long pt,
        @RequestParam(value = "num") int num,
        @RequestParam(value = "front") boolean front
    ) {
        LinkedList<Long> out = new LinkedList<>();
        System.out.println(""+pid+" "+pt+" "+num+ " " +front);

        int delta = front ? -1 : 1;
        try {
            while (num --> 0) {
                pt += delta;
                if (replyListService.contain(pid, pt)) {
                    out.add(pt);
                }
            }
        } catch (PidNotExistException ignored) {

        } catch (Exception e) {
            logger.error(toString() +
                    ".getList(" + pt + ", " + num + ", " + front + ")");
        }

        return out;
    }
}
