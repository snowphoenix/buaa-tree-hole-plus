package cn.edu.buaa.treehole.common;


import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeFormatter {
    private static final String pattern = "yyyy/MM/dd-HH:mm";

    private static final SimpleDateFormat formatter = new SimpleDateFormat(pattern);

    public static String format(Date time) {
        return formatter.format(time);
    }

    public static String nowTime() {
        return formatter.format(new Date());
    }
}
