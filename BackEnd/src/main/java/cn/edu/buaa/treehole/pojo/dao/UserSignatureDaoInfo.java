package cn.edu.buaa.treehole.pojo.dao;

import lombok.Data;

import java.util.Date;

@Data
public class UserSignatureDaoInfo {
    private long pid;
    private String signature;
    private long uid;
    private Date time;

    public UserSignatureDaoInfo() {}

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getUid() {
        return uid;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getTime() {
        return time;
    }
}
