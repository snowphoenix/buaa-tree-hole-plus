package cn.edu.buaa.treehole.pojo.network.post;

/*
 * @author Deng XinYu
 * */
public class DeletePostResponseNetworkInfo {
    private String status;
    private String time;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
