package cn.edu.buaa.treehole.dao.exception;

public class SignatureNotExistException extends DaoException {
    public SignatureNotExistException() {
    }

    public SignatureNotExistException(String msg) {
        super(msg);
    }

    public SignatureNotExistException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public SignatureNotExistException(Throwable cause) {
        super(cause);
    }

    protected SignatureNotExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
