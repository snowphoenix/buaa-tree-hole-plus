package cn.edu.buaa.treehole.pojo.dao;

import lombok.Data;

@Data
public class TagDaoInfo {
    private long tid;
    private String name;
    private String description;

    public TagDaoInfo() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTid(long tid) {
        this.tid = tid;
    }

    public String getDescription() {
        return description;
    }

    public long getTid() {
        return tid;
    }
}
