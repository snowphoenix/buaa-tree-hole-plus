package cn.edu.buaa.treehole.pojo.network.user;

import lombok.Data;

@Data
public class UserLoginRequestInfo {
    String email;
    String password;

    public UserLoginRequestInfo() {}

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
