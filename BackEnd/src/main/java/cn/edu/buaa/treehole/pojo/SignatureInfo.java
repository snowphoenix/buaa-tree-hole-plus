package cn.edu.buaa.treehole.pojo;

import java.util.Objects;

/*
 * @author Deng XinYu
 * */
public class SignatureInfo {
    private long pid;
    private String signature;
    private String name;

    public SignatureInfo() { }

    public SignatureInfo(long pid, String signature, String name) {
        this.pid = pid;
        this.signature = signature;
        this.name = name;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SignatureInfo that = (SignatureInfo) o;
        return pid == that.pid &&
                Objects.equals(signature, that.signature) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pid, signature, name);
    }

    @Override
    public String toString() {
        return "SignatureInfo{" +
                "pid=" + pid +
                ", signature='" + signature + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
