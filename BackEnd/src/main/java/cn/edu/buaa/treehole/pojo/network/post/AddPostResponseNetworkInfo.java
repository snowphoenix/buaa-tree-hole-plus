package cn.edu.buaa.treehole.pojo.network.post;

/*
 * @author Deng XinYu
 * */
public class AddPostResponseNetworkInfo {
    private String status;
    private long pid;
    private String time;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
