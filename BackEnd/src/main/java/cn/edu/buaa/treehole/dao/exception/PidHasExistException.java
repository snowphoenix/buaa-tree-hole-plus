package cn.edu.buaa.treehole.dao.exception;

/*
 * @author Deng XinYu
 * */
public class PidHasExistException extends DaoException {
    public PidHasExistException() {
    }

    public PidHasExistException(String msg) {
        super(msg);
    }

    public PidHasExistException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public PidHasExistException(Throwable cause) {
        super(cause);
    }

    protected PidHasExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
