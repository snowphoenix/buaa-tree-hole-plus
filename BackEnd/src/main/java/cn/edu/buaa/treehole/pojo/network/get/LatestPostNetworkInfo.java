package cn.edu.buaa.treehole.pojo.network.get;

import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * @author Deng XinYu
 * */
public class LatestPostNetworkInfo {
    private String status;
    private long pid;

    @JsonProperty("fresh_time")
    private String freshTime;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public String getFreshTime() {
        return freshTime;
    }

    public void setFreshTime(String freshTime) {
        this.freshTime = freshTime;
    }
}
