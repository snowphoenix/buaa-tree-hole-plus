package cn.edu.buaa.treehole.dao.impl;

import cn.edu.buaa.treehole.dao.PostTagDao;
import cn.edu.buaa.treehole.dao.dbutil.DbAccess;
import cn.edu.buaa.treehole.dao.dbutil.DbConnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PostTagDaoImpl implements PostTagDao {

    private final DbConnection conn = DbConnection.getInstance(
            DbAccess.url,
            DbAccess.usr,
            DbAccess.pwd
    );

    @Override
    public void insertLinkPidWithTid(long pid, long tid) {
        try {
            Statement statement = this.conn.createStatement();
            ResultSet rs = statement.executeQuery(String.format(
                    "SELECT * FROM postag WHERE pid=%d AND tid=%d",
                    pid,
                    tid
            ));
            if (rs.next()) {
                rs.close();
                statement.close();
                throw new RuntimeException("PidAndTidExistingException: " +
                        "pid = " + pid + ", tid = " + tid);
            }
            if (statement.executeUpdate(String.format(
                    "INSERT INTO postag (pid, tid) VALUES (%d, %d)",
                    pid,
                    tid
            )) == 0) {
                rs.close();
                statement.close();
                throw new RuntimeException("Insertion into database failed.");
            }
            rs.close();
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException(
                    "This Exception is caused by SQLException: " + e.getCause() +
                            ", SQL State: " + e.getSQLState() +
                            ", Error Code: " + e.getErrorCode()
            );
        }
    }

    @Override
    public List<Long> selectAllTidByPid(long pid) {
        try {
            Statement statement = this.conn.createStatement();
            ResultSet rs = statement.executeQuery(String.format(
                    "SELECT * FROM postag WHERE pid=%d",
                    pid
            ));
            List<Long> list = new ArrayList<>();
            while (rs.next()) {
                list.add(rs.getLong("tid"));
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(
                    "This Exception is caused by SQLException: " + e.getCause() +
                            ", SQL State: " + e.getSQLState() +
                            ", Error Code: " + e.getErrorCode()
            );
        }
    }

    @Override
    public void deleteAllByPid(long pid) {
        try {
            Statement statement = this.conn.createStatement();
            ResultSet rs = statement.executeQuery(String.format(
                    "SELECT * FROM postag WHERE pid=%d",
                    pid
            ));
            if (!rs.next()) {
                rs.close();
                statement.close();
                throw new RuntimeException("PidNotExistingException: pid = " + pid);
            }
            if (statement.executeUpdate(String.format(
                    "DELETE FROM postag WHERE pid=%d",
                    pid
            )) == 0) {
                rs.close();
                statement.close();
                throw new RuntimeException("Deletion from database failed.");
            }
            rs.close();
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException(
                    "This Exception is caused by SQLException: " + e.getCause() +
                            ", SQL State: " + e.getSQLState() +
                            ", Error Code: " + e.getErrorCode()
            );
        }
    }

    @Override
    public void deleteCertain(long pid, long tid) {
        try {
            Statement statement = this.conn.createStatement();
            ResultSet rs = statement.executeQuery(String.format(
                    "SELECT * FROM postag WHERE pid=%d AND tid=%d",
                    pid,
                    tid
            ));
            if (!rs.next()) {
                rs.close();
                statement.close();
                throw new RuntimeException("PidAndTidNotExistingException: " +
                        "pid = " + pid + ", tid = " + tid);
            }
            if (statement.executeUpdate(String.format(
                    "DELETE FROM postag WHERE pid=%d AND tid=%d",
                    pid,
                    tid
            )) == 0) {
                rs.close();
                statement.close();
                throw new RuntimeException("Deletion from database failed.");
            }
            rs.close();
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException(
                    "This Exception is caused by SQLException: " + e.getCause() +
                            ", SQL State: " + e.getSQLState() +
                            ", Error Code: " + e.getErrorCode()
            );
        }
    }
}
