package cn.edu.buaa.treehole.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostTagDao {
    @Insert("insert into postag (pid, tid) values (#{pid}, #{tid})")
    void insertLinkPidWithTid(@Param("pid") long pid, @Param("tid") long tid);

    @Select("select tid from postag where pid=#{pid}")
    List<Long> selectAllTidByPid(@Param("pid") long pid);

    @Delete("delete from postag where pid=#{pid}")
    void deleteAllByPid(@Param("pid") long pid);

    @Delete("delete from postag where pid=#{pid} and tid=#{tid}")
    void deleteCertain(@Param("pid") long pid, @Param("tid") long tid);
}
