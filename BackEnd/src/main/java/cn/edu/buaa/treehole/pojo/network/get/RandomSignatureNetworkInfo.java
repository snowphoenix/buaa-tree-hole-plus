package cn.edu.buaa.treehole.pojo.network.get;

/*
 * @author Deng XinYu
 * */
public class RandomSignatureNetworkInfo {
    private String status;
    private String time;
    private String signature;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
