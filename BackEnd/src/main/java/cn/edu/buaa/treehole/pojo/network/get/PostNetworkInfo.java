package cn.edu.buaa.treehole.pojo.network.get;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

/*
 * @author Deng XinYu
 * */
@Data
public class PostNetworkInfo {
    private String status;
    private long pid;
    private String title;
    private String content;
    private String time;
    private Map<Long, String> tags;

    @JsonProperty(value = "reply_number")
    private int replyNumber;
}
