package cn.edu.buaa.treehole.controller.post;

import cn.edu.buaa.treehole.service.TagService;
import cn.edu.buaa.treehole.service.TimeService;
import cn.edu.buaa.treehole.dao.exception.DaoException;
import cn.edu.buaa.treehole.dao.exception.SignatureNotExistException;
import cn.edu.buaa.treehole.pojo.PostInfo;
import cn.edu.buaa.treehole.pojo.network.post.AddPostRequestNetworkInfo;
import cn.edu.buaa.treehole.pojo.network.post.AddPostResponseNetworkInfo;
import cn.edu.buaa.treehole.pojo.network.post.DeletePostRequestNetworkInfo;
import cn.edu.buaa.treehole.pojo.network.post.DeletePostResponseNetworkInfo;
import cn.edu.buaa.treehole.service.PostInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;

/*
 * @author Deng XinYu
 * */
@Controller
public class AddDeletePostController {
    private final PostInfoService postInfoService;
    private final TimeService timeService;
    private final TagService tagService;

    @Autowired
    public AddDeletePostController(PostInfoService postInfoService, TimeService timeService, TagService tagService) {
        this.postInfoService = postInfoService;
        this.timeService = timeService;
        this.tagService = tagService;
    }

    @RequestMapping(value = "/add_post", method = RequestMethod.POST)
    @ResponseBody
    public AddPostResponseNetworkInfo addPost(@RequestBody AddPostRequestNetworkInfo in) {
        AddPostResponseNetworkInfo out = new AddPostResponseNetworkInfo();
        System.out.println("add post");
        PostInfo info = new PostInfo();
        info.setTitle(in.getTitle());
        info.setContent(in.getContent());
        info.setSignature(in.getSignature());
        Date time = new Date();
        info.setDate(time);
        out.setTime(timeService.format(time));
        try {
            ArrayList<Long> tids = new ArrayList<>();
            for (String name : in.getTags()) {
                tids.add(tagService.ensureTagOfName(name));
            }
            info.setTidList(tids);
            long pid = postInfoService.addPost(info);
            out.setPid(pid);
            out.setStatus("success");
        } catch (Exception e) {
            System.out.println(e.toString());
            out.setStatus("fail");
        }

        return out;
    }

    @RequestMapping(value = "/delete_post", method = RequestMethod.POST)
    @ResponseBody
    public DeletePostResponseNetworkInfo deletePost(@RequestBody DeletePostRequestNetworkInfo in) {
        DeletePostResponseNetworkInfo out = new DeletePostResponseNetworkInfo();
        System.out.println("delete post "+ in.getPid() + in.getSignature());
        out.setTime(timeService.nowTime());
        try {
            postInfoService.deletePost(in.getPid(), in.getSignature());
            out.setStatus("success");
        } catch (SignatureNotExistException e) {
            out.setStatus("wrong");
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
            out.setStatus("exception");
        }

        return out;
    }
}
