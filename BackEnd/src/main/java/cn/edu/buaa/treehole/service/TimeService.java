package cn.edu.buaa.treehole.service;


import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class TimeService {
    private final String pattern = "yyyy/MM/dd-HH:mm";

    private final SimpleDateFormat formatter = new SimpleDateFormat(pattern);

    public int getDeltaHour(Date old) {
        return getDeltaHour(new Date(), old);
    }

    public int getDeltaHour(Date now, Date old) {
        long delta = now.getTime() - old.getTime();
        return (int) (delta / (1000 * 60 * 60));
    }

    public String format(Date time) {
        return formatter.format(time);
    }

    public String nowTime() {
        return formatter.format(new Date());
    }
}
