package cn.edu.buaa.treehole.service;

import cn.edu.buaa.treehole.dao.ReplyDao;
import cn.edu.buaa.treehole.dao.exception.DaoException;
import cn.edu.buaa.treehole.dao.exception.PidNotExistException;
import cn.edu.buaa.treehole.pojo.dao.ReplyDaoInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;

@Service
public class ReplyListService {
    private final ReplyDao replyDao;

    @Autowired
    public ReplyListService(ReplyDao replyDao) {
        this.replyDao = replyDao;
    }


    public long getEarliestReply(long pid) throws PidNotExistException, DaoException {
        return replyDao.selectMinRidByPid(pid);
    }

    public long getLatestReply(long pid) throws PidNotExistException, DaoException {
        return replyDao.selectMaxRidByPid(pid);
    }

    public boolean contain(long pid, long rid) throws DaoException {
        try {
            ReplyDaoInfo ret = replyDao.selectReplyById(pid, rid);
            return ret != null;
        } catch (DataRetrievalFailureException e) {
            return false;
        }
    }
}
