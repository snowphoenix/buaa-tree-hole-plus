package cn.edu.buaa.treehole.dao;

import cn.edu.buaa.treehole.pojo.dao.UserDaoInfo;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao {
    @Insert("insert into user (uid, email, password, auto) " +
            "values(#{uid}, #{email}, #{password}, #{auto})")
    void insertUser(UserDaoInfo userDaoInfo);

    @Update("update user set auto=#{signature}")
    void updateDefaultSignature(@Param("uid") long uid, @Param("signature") String signature);

    @Update("update user set password=#{password}")
    void updatePassword(@Param("uid") long uid, @Param("password") String password);

    @Select("select * from user where uid=#{uid}")
    UserDaoInfo selectUserByUid(@Param("uid") long uid);

    @Select("select * from user where email=#{email}")
    UserDaoInfo selectUserByEmail(@Param("email") String email);

    @Select("select * from user where email=#{email}")
    List<UserDaoInfo> selectAllUserByEmail(@Param("email") String email);

    @Select("select max(uid) from user")
    long selectMaxUid();
}
