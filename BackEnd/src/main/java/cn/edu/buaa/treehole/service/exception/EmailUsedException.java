package cn.edu.buaa.treehole.service.exception;

public class EmailUsedException extends Exception {
    public EmailUsedException() {
        super();
    }

    public EmailUsedException(String msg) {
        super(msg);
    }

    public EmailUsedException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public EmailUsedException(Throwable cause) {
        super(cause);
    }
}