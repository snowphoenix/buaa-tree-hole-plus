package cn.edu.buaa.treehole.pojo;

import java.util.Date;
import java.util.Objects;

/*
 * @author Deng XinYu
 * */
public class ReplyInfo {
    private long pid;
    private long rid;
    private String content;
    private String Signature;
    private Date date;

    public ReplyInfo() { }

    public ReplyInfo(long pid, long rid, String content, String signature, Date date) {
        this.pid = pid;
        this.rid = rid;
        this.content = content;
        Signature = signature;
        this.date = date;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public long getRid() {
        return rid;
    }

    public void setRid(long rid) {
        this.rid = rid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSignature() {
        return Signature;
    }

    public void setSignature(String signature) {
        Signature = signature;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReplyInfo replyInfo = (ReplyInfo) o;
        return pid == replyInfo.pid &&
                rid == replyInfo.rid &&
                Objects.equals(content, replyInfo.content) &&
                Objects.equals(Signature, replyInfo.Signature) &&
                Objects.equals(date, replyInfo.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pid, rid, content, Signature, date);
    }

    @Override
    public String toString() {
        return "ReplyInfo{" +
                "pid=" + pid +
                ", rid=" + rid +
                ", content='" + content + '\'' +
                ", Signature='" + Signature + '\'' +
                ", date=" + date +
                '}';
    }
}
