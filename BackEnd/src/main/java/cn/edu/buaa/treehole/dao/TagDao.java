package cn.edu.buaa.treehole.dao;

import cn.edu.buaa.treehole.pojo.dao.TagDaoInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagDao {
    @Select("select * from tag where tid=#{tid}")
    TagDaoInfo selectTagByTid(@Param("tid") long tid);

    @Select("select * from tag")
    List<TagDaoInfo> selectAllTag();

    @Insert("insert into tag (tid, name, description) values (#{tid}, #{name}, #{description})")
    void insertTag(TagDaoInfo tagDaoInfo);

    @Select("select max(tid) from tag")
    Long selectMaxTid();
}
