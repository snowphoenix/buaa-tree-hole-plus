package cn.edu.buaa.treehole.pojo.dao;

import lombok.Data;

@Data
public class ContentDaoInfo {
    private long pid;
    private long rid;
    private long ord;
    private String contents;
}
