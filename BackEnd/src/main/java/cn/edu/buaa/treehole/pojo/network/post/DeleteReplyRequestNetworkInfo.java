package cn.edu.buaa.treehole.pojo.network.post;

/*
 * @author Deng XinYu
 * */
public class DeleteReplyRequestNetworkInfo {
    private long pid;
    private long rid;
    private String signature;

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public long getRid() {
        return rid;
    }

    public void setRid(long rid) {
        this.rid = rid;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
