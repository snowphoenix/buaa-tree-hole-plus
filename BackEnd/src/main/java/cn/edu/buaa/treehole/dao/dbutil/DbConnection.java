package cn.edu.buaa.treehole.dao.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/*
 * @author Zhu Leyan
 * */
public class DbConnection {

    private static final Map<Integer, DbConnection> map = new HashMap<>();

    private static final String DRIVER = "com.mysql.jdbc.driver";
    private final String url;
    private final String user;
    private final String pwd;

    private Connection conn;

    public static DbConnection getInstance(String url, String user, String pwd) {
        if (map.containsKey(Objects.hash(url, user, pwd))) {
            return map.get(Objects.hash(url, user, pwd));
        }
        DbConnection newInstance = new DbConnection(url, user, pwd);
        map.put(Objects.hash(url, user, pwd), newInstance);
        return newInstance;
    }

    /**
     * Constructs a new MySQL database connection manager.
     * The connection is not established, use {@code open} for initializing connection.
     * @param url The url of database.
     * @param user The username for database accessing.
     * @param pwd The corresponding password to {@code user}.
     */
    private DbConnection(String url, String user, String pwd) {
        this.url = url;
        this.user = user;
        this.pwd = pwd;
    }

    /**
     * Get the connection instance, which should have been established
     * using {@code open}.
     * @return The connection instance.
     * @throws SQLException if error occurs during the process.
     */
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(this.url, this.user, this.pwd);
    }

    /**
     * Creates a Statement object for sending SQL statements to the database.
     * @return a new default Statement object
     * @throws SQLException if error occurs during the process.
     */
    public Statement createStatement() throws SQLException {
        return this.getConnection().createStatement();
    }
}
