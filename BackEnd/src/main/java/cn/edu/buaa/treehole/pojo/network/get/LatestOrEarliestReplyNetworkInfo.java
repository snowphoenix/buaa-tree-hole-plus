package cn.edu.buaa.treehole.pojo.network.get;

import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * @author Deng XinYu
 * */
public class LatestOrEarliestReplyNetworkInfo {
    private String status;

    @JsonProperty(value = "fresh_time")
    private String freshTime;
    private long rid;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFreshTime() {
        return freshTime;
    }

    public void setFreshTime(String freshTime) {
        this.freshTime = freshTime;
    }

    public long getRid() {
        return rid;
    }

    public void setRid(long rid) {
        this.rid = rid;
    }
}
