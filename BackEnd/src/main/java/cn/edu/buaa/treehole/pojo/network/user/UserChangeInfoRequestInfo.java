package cn.edu.buaa.treehole.pojo.network.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UserChangeInfoRequestInfo {
    @JsonProperty("old_password")
    private String oldPassword;

    @JsonProperty("new_password")
    private String newPassword;

    @JsonProperty("default_signature")
    private String defaultSignature;

    public UserChangeInfoRequestInfo() {}

    public String getDefaultSignature() {
        return defaultSignature;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setDefaultSignature(String defaultSignature) {
        this.defaultSignature = defaultSignature;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
