package cn.edu.buaa.treehole.pojo.network.post;

/*
 * @author Deng XinYu
 * */
public class AddReplyResponseNetworkInfo {
    private String status;
    private long rid;
    private String name;
    private String time;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getRid() {
        return rid;
    }

    public void setRid(long rid) {
        this.rid = rid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
