package cn.edu.buaa.treehole.pojo;

import lombok.Data;

import java.util.Date;
import java.util.Map;

@Data
public class PostInfoWithTagMap {
    private long pid;
    private String title;
    private String content;
    private Date time;
    private Map<Long, String> tags;

    public PostInfoWithTagMap() {}

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Map<Long, String> getTags() {
        return tags;
    }

    public void setTags(Map<Long, String> tags) {
        this.tags = tags;
    }
}
