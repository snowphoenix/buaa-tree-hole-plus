package cn.edu.buaa.treehole.pojo.network.user;

import lombok.Data;

@Data
public class UserDeleteSignatureRequestInfo {
    private long pid;
    private String signature;

    public UserDeleteSignatureRequestInfo() {}

    public void setPid(long pid) {
        this.pid = pid;
    }

    public long getPid() {
        return pid;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
