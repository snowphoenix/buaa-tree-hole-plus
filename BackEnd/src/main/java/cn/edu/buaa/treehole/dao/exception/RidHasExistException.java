package cn.edu.buaa.treehole.dao.exception;

/*
 * @author Deng XinYu
 * */
public class RidHasExistException extends DaoException {
    public RidHasExistException() {
    }

    public RidHasExistException(String msg) {
        super(msg);
    }

    public RidHasExistException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public RidHasExistException(Throwable cause) {
        super(cause);
    }

    protected RidHasExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
