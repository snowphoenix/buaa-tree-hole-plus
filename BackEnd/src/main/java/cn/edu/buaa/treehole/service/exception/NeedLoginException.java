package cn.edu.buaa.treehole.service.exception;

public class NeedLoginException extends Exception {
    public NeedLoginException() {
        super();
    }

    public NeedLoginException(String msg) {
        super(msg);
    }

    public NeedLoginException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public NeedLoginException(Throwable cause) {
        super(cause);
    }
}
