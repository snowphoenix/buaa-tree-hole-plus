package cn.edu.buaa.treehole.pojo.network.user;

import lombok.Data;

@Data
public class UserSignatureRequestInfo {
    private String type;
    private long pid;
}
